//
//  Manager.swift
//  Hai In Bucovina
//
//  Created by Alexx on 03/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation


protocol ManagerDelegate: class {
    func sendUserData(_ user: String)
    func checkAuth(_ auth: String!)
    func observeUserConversations(_ users: String)
    func observeMessages(senderId: String, text: String, date: String)
    func observeMessagesWithData(senderId: String, data: Data, text: String)
    func observeTyping(_ count: Int)
    func addUser(_ user: String)
    func removeUser(_ user: String)
}

extension ManagerDelegate {
    func sendUserData(_ user: String) {
    }
    func checkAuth(_ auth: String!) {
    }
    func observeUserConversations(_ users: String) {
    }
    func observeMessages(senderId: String, text: String, date: String) {
    }
    func observeMessagesWithData(senderId: String, data: Data, text: String) {
    }
    func observeTyping(_ count: Int) {
    }
    func addUser(_ user: String) {
    }
    func removeUser(_ user: String) {
    }
}


class Manager: NSObject, ServerDelegate {
    
    weak var delegate: ManagerDelegate?
    private let serverManager = Server()

    
    func login(email: String, password: String) {
        serverManager.delegate = self
        serverManager.login(email: email, password: password)
    }
    
    func signUp(email: String, password: String) {
        serverManager.createUser(email: email, password: password)
    }
    
    func signOut(_ email: String) {
        serverManager.signOut(email)
    }
    
    func listenerForUserAuth() {
        serverManager.delegate = self
        serverManager.listenForUserAuth()
    }
    
    func checkCredentials() -> (String, String) {
        let id = serverManager.sendCredentialsForAuthUser().0
        let name = serverManager.sendCredentialsForAuthUser().1
        return (id, name)
    }
    
    func sendMessage(child: String, text: String, senderId: String, date: Date) {
        serverManager.sendMessage(child: child, text: text, senderId: senderId, date: date)
    }
    
    func sendFile(data: Data, senderId:String, text: String) {
        serverManager.sendFile(data: data, senderId: senderId, text: text)
    }
    
    func observeCurrentUserConversations(senderId: String) {
        serverManager.delegate = self
        serverManager.observeCurrentUserConversations(senderId: senderId)
    }
    func observeMessage(child: String) {
        serverManager.delegate = self
        serverManager.observeMessage(child: child)
    }
    
    func observeTyping(senderId: String, typing: Bool) {
        serverManager.delegate = self
        serverManager.observeTyping(senderId: senderId, typing: typing)
    }
    
    func checkLoggedUsers() {
        serverManager.delegate = self
        serverManager.checkLoggedUsers()
    }
    
    func removeObserverFromServer(child: String) {
        serverManager.removeObservers(child: child)
    }
    //MARK: - server delegate protocol for user auth changes
    
    func sendUserData(_ user: String) {
        self.delegate?.sendUserData(user)
    }
    
    func checkAuth(_ auth: String!) {
        self.delegate?.checkAuth(auth)
    }
    
    func observeMessages(senderId: String, text: String, date: String) {
        self.delegate?.observeMessages(senderId: senderId, text: text, date: date)
    }
    
    func observeMessagesWithData(senderId: String, data: Data, text: String) {
        self.delegate?.observeMessagesWithData(senderId: senderId, data: data, text: text)
    }
    
    func observeTyping(_ count: Int) {
        self.delegate?.observeTyping(count)
    }
    
    func addUser(_ user: String) {
        self.delegate?.addUser(user)
    }
    
    func removeUser(_ user: String) {
        self.delegate?.removeUser(user)
    }
    func observeUserConversations(_ users: String) {
        self.delegate?.observeUserConversations(users)
    }
}
