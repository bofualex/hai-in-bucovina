//
//  CreateDismissChildControllers.swift
//  Hai In Bucovina
//
//  Created by Alexx on 02/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

class ChildControllers: NSObject {
    
    //MARK: - create childViewControllers
    
    class func createChild(currentView: UIViewController, name: String) -> UIViewController {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
        currentView.addChildViewController(viewController)
        currentView.view.addSubview(viewController.view)
        let viewsDict = ["child" : viewController.view] as [String: AnyObject]
        currentView.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        currentView.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        viewController.didMove(toParentViewController: currentView)
        return viewController
    }
    
    //MARK: - dismiss childViewControllers
    
    class func dismissChild(currentView: UIViewController) {
        currentView.willMove(toParentViewController: nil)
        currentView.view.removeFromSuperview()
        currentView.removeFromParentViewController()
    }
    
    //MARK: - create alert controller
    
    class func errorAlert(_ currentView: UIViewController, title: String, message: String) {
        let errorAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        errorAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        errorAlert.view.alpha = 0.7
        currentView.present(errorAlert, animated: true, completion: nil)
    }

    
}
