//
//  ViewController.swift
//  Hai In Bucovina
//
//  Created by Alexx on 26/07/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import UIKit
import AVFoundation

class MainScreen: UIViewController, ManagerDelegate {
    
    
    @IBOutlet private weak var menuButton: UIButton!
    @IBOutlet private weak var logInButton: UIButton!
    @IBOutlet private weak var userName: UILabel?
    @IBOutlet private weak var soundButton: UIButton?
    
    private lazy var buttons: [UIButton] = []
    private lazy var whereToButtons: [UIButton] = []
    
    private lazy var constraintsTop: [NSLayoutConstraint] = []
    private lazy var constraintsRight: [NSLayoutConstraint] = []
    private lazy var constraintsTopWhereTo: [NSLayoutConstraint] = []
    private lazy var constraintsRightWhereTo: [NSLayoutConstraint] = []
    
    private var buttonState: Int = 1
    private var audioState: Int = 0
    private let manager = Manager()
    
    //MARK: - view methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        manager.listenerForUserAuth()
        initMenuButtons()
        menuButton.addTarget(self, action: #selector(animateMenuButtons), for: .touchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - action method linked to the sports button
    
    @IBAction private func pressedButton(sender: UIButton) {
        let child = ChildControllers.createChild(currentView: self, name: "SafariVC") as! SafariVC
        child.buttonIdentifier = sender.restorationIdentifier
        print(child.buttonIdentifier)
    }
    
    //MARK: - method to create login view
    
    @IBAction private func loginButtonTapped(sender: UIButton) {
        if logInButton.title(for: .normal) == "Log Out" {
            logInButton.setTitle("Log In", for: .normal)
            self.viewDidLoad()
            manager.signOut(manager.checkCredentials().1)
        }
    }
    
    @IBAction private func membersAreaPressed(sender: UIButton) {
        if userName?.text == " Welcome anonymous user" {
            _ = ChildControllers.errorAlert(self, title: "Forbidden", message: "You have to be logged in to access this area")
        }
    }
    
    //MARK: - right menu buttons methods
    
    func initMenuButtons() {
        buttons = ButtonsAndConstraints.generateButtons(self.view, nrOfButtons: 5)
        whereToButtons = ButtonsAndConstraints.generateButtonsFromFile(self.view, buttonType: "whereto", backColor: .lightGray, dimension: 40)
        let titles = ["Where to?", "Nature", "Events", "About", "Review"]
        for i in 0..<buttons.count {
            buttons[i].setTitle(titles[i], for: .normal)
            buttons[i].accessibilityIdentifier = titles[i]
        }
        ButtonsAndConstraints.setButtonsAlpha(buttons, alpha: 0)
        ButtonsAndConstraints.setButtonsAlpha(whereToButtons, alpha: 0)
        constraintsTop = ButtonsAndConstraints.generateConstraintsTop(self.view, button1: buttons, constant: 50)
        constraintsRight = ButtonsAndConstraints.generateConstraintsRight(self.view, button1: buttons, constant: -10)
        constraintsTopWhereTo = ButtonsAndConstraints.generateConstraintsTop(self.view, button1: whereToButtons, constant: 120)
        constraintsRightWhereTo = ButtonsAndConstraints.generateConstraintsRight(self.view, button1: whereToButtons, constant: -40)
        buttons[0].addTarget(self, action: #selector(wheretoButtonPressed), for: .touchUpInside)
        buttons[4].addTarget(self, action: #selector(aboutButtonPressed), for: .touchUpInside)
    }
    
    @objc private func animateMenuButtons() {
        guard (buttonState % 2) == 0 else {
            ButtonsAndConstraints.setConstraints1(constraintsTop, radius: 0, x: -60, y: 37)
            UIView .animate(withDuration: 0.3) { [unowned self] in
                self.view.layoutIfNeeded()
                ButtonsAndConstraints.setButtonsAlpha((self.buttons), alpha: 1)
            }
            buttonState += 1
            return
        }
        ButtonsAndConstraints.resetConstraints(constraintsTop, constant: 50)
        ButtonsAndConstraints.resetConstraints(constraintsRightWhereTo, constant: -40)
        UIView .animate(withDuration: 0.3) { [unowned self] in
            self.view.layoutIfNeeded()
            ButtonsAndConstraints.setButtonsAlpha((self.buttons), alpha: 0)
            ButtonsAndConstraints.setButtonsAlpha((self.whereToButtons), alpha: 0)
        }
        buttonState += 1
    }
    
    @objc private func wheretoButtonPressed(sender: UIButton) {
        guard (buttonState % 2) == 1 else {
            ButtonsAndConstraints.setConstraints1(constraintsRightWhereTo, radius: 0, x: 30, y: -47)
            UIView .animate(withDuration: 0.3) { [unowned self] in
                self.view.layoutIfNeeded()
                ButtonsAndConstraints.setButtonsAlpha((self.buttons), alpha: 1)
                ButtonsAndConstraints.setButtonsAlpha((self.whereToButtons), alpha: 1)
            }
            return
        }
        ButtonsAndConstraints.resetConstraints(constraintsRightWhereTo, constant: -40)
        UIView .animate(withDuration: 0.3) { [unowned self] in
            self.view.layoutIfNeeded()
            ButtonsAndConstraints.setButtonsAlpha((self.buttons), alpha: 0)
        }
    }
    
    @IBAction private func playSound() {
        var player: AVAudioPlayer?
        let url = Bundle.main.url(forResource: "1", withExtension: "mp3")!
        do {
            player = try AVAudioPlayer(contentsOf: url)
            guard player != nil else { return }
        } catch let error as NSError {
            print(error.description)
        }
        switch (audioState % 2) {
        case 0:
            player?.prepareToPlay()
            player?.play()
        default:
            player?.stop()
        }
        audioState += 1
    }

    //MARK: - listenForUser protocol implementation
    
    func sendUserData(_ user: String)  {
        if user == "not logged in" {
            userName?.text = " Welcome anonymous user"
            logInButton.setTitle("Log In", for: .normal)
        } else {
            userName?.text = " Welcome " + user.components(separatedBy: "@").first!.capitalized
            logInButton.setTitle("Log Out", for: .normal)
        }
    }
    
    //MARK: - segue to membersarea
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "membersArea" {
            super.prepare(for: segue, sender: sender)
            _ = segue.destination as? MembersArea
        }
        manager.removeObserverFromServer(child: "membersLogged")
    }
    
    func aboutButtonPressed() {
        _ = ChildControllers.createChild(currentView: self, name: "Review") as? Review
        animateMenuButtons()
    }
}

extension Date {
    func dateFromString(_ date: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let dateToDisplay = formatter.date(from: date)
        return dateToDisplay!
    }
    
    func formatDate(_ date: Date) -> String {
        let formatter = DateFormatter()
        let day_seconds: TimeInterval = -86400
        if date.timeIntervalSinceNow < day_seconds {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            let dateToDisplay = formatter.string(from: date)
            return dateToDisplay
        } else {
            formatter.timeStyle = .medium
            let dateToDisplay = formatter.string(from: date)
            return dateToDisplay
        }
    }
}

