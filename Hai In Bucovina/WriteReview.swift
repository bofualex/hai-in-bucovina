//
//  WriteReview.swift
//  Hai In Bucovina
//
//  Created by Alexx on 06/09/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

protocol WriteReviewDelegate: class {
    func reloadReview(_ text: String)
}

class WriteReview: UIViewController {
    
    @IBOutlet weak var nameLabel: UITextField?
    @IBOutlet weak var textToSend: UITextView?
    
    let manager = Manager()
    weak var delegate: WriteReviewDelegate?
    
    var flag: String = "false"
//    var sentReview: String {
//        get {
//            return flag
//        }
//        set {
//            flag = newValue
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func pressedSendButton() {
        guard (nameLabel?.text?.characters.count)! > 4 else {
            ChildControllers.errorAlert(self, title: "User Name too short", message: "Please enter a name longer than 5 characters")
            return
        }
        guard (textToSend?.text?.characters.count)! > 10 else {
            ChildControllers.errorAlert(self, title: "Text too short", message: "Please enter a text longer than 10 characters")
            return
        }
        manager.sendMessage(child: "anonym", text: (textToSend?.text)!, senderId: (nameLabel?.text)!, date: Date())
        nameLabel?.text = ""
        textToSend?.text = ""
        flag = "succes"
        //reloadViewController()
    }
    
    func reloadViewController() {
        guard flag == "false" else {
            delegate?.reloadReview("success")
            return
        }
    }
}
