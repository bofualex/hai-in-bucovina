//
//  SportsViewController.swift
//  Hai In Bucovina
//
//  Created by Alexx on 03/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class SafariVC: UIViewController {
    
    typealias constraints = [NSLayoutConstraint]

    private lazy var buttonArrayConstraintsTop: constraints = []
    private lazy var buttonArrayConstraintsLeft: constraints = []
    
    private lazy var buttonArray: [UIButton] = []
    private lazy var buttonArrayConstraints: constraints = []
    
    var buttonIdentifier: String?
    
    //MARK: - view methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generateInitialView()
        print(buttonIdentifier)
        _ = Timer.scheduledTimer(timeInterval: 0.5, target:self, selector: #selector(displaySports), userInfo: nil, repeats: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - generate buttons and their constraints and adding target to them
    
    private func generateInitialView() {
        buttonArray = ButtonsAndConstraints.generateButtonsFromFile(self.view, buttonType: "sports", backColor: UIColor.yellow, dimension: 80)
        buttonArrayConstraintsLeft = ButtonsAndConstraints.generateConstraintsLeft(self.view, button1: buttonArray, constant: 152)
        buttonArrayConstraintsTop = ButtonsAndConstraints.generateConstraintsTop(self.view, button1: buttonArray, constant: 323)
        ButtonsAndConstraints.setButtonsAlpha(buttonArray, alpha: 0)
        for button in buttonArray {
            button.addTarget(self, action: #selector(sendButtonIdentifier), for: .touchUpInside)
        }
    }
    
    //MARK: - reset constraints and alpha for buttons
    
    private func resetGeneratedButtons() {
        ButtonsAndConstraints.resetConstraints(buttonArrayConstraintsTop, constant: 323)
        ButtonsAndConstraints.resetConstraints(buttonArrayConstraintsLeft, constant: 152)
    }
    
    //MARK: - presenting safari content depending on which sports button was pressed
    
    @objc private func sendButtonIdentifier(sender: UIButton) {
        var url: String = "https://www.facebook.com/haiinbucovina/?fref=pb&hc_location=profile_browser"
        for sport in Sports.sportsArray {
            if sport.0 == sender.accessibilityIdentifier {
                url = sport.1
                print(url)
            }
        }
        if let url = URL(string: url) {
            let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            present(vc, animated: true, completion: nil)
        }
    }
    
    @objc private func displaySports() {
        ButtonsAndConstraints.setConstraints(buttonArrayConstraintsTop, constraint2: buttonArrayConstraintsLeft, radius: 120)
        UIView .animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            ButtonsAndConstraints.setButtonsAlpha(self.buttonArray, alpha: 1)
        }
    }

    //MARK: - action method for dismissing the viewcontroller
    
    @IBAction private func dismissChild() {
        resetGeneratedButtons()
        UIView .animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            ButtonsAndConstraints.setButtonsAlpha(self.buttonArray, alpha: 0)
        }
        ChildControllers.dismissChild(currentView: self)
    }
}

