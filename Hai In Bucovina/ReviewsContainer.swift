//
//  ReviewsContainer.swift
//  Hai In Bucovina
//
//  Created by Alexx on 06/09/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

typealias ReviewFromUser = [(senderId:String, text:String, date:String)]

class ReviewsContainer: UIViewController, ManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    private let manager = Manager()
    @IBOutlet weak var tableReview: UITableView?
    private lazy var reviewList: ReviewFromUser = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        manager.observeMessage(child: "anonym")
        tableReview?.dataSource = self
        tableReview?.delegate = self
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard reviewList.count == 0 else {
            return reviewList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ReviewsContainerCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ReviewsContainerCell
        let review = reviewList[indexPath.row]
        cell.nameReview?.text = review.senderId
        cell.textReview?.text = review.text
//        if (cell.textReview?.numberOfLines)! >= 5 {
//            cell.moreButton?.alpha = 1
//        } else {
//            cell.moreButton?.alpha = 0
//        }
        cell.dateReview?.text = Date().formatDate(Date().dateFromString(review.date))
        return cell
    }
    
    //MARK: - observe message protocol implementation
    
    func observeMessages(senderId: String, text: String, date: String) {
        reviewList.append((senderId, text, date))
        reviewList.sort(by: {$1<$0})
        tableReview?.reloadData()
    }
}

