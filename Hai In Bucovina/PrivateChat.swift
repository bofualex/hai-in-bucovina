//
//  PrivateChat.swift
//  Hai In Bucovina
//
//  Created by Alexx on 02/09/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import JSQMessagesViewController


class PrivateChatMainScreen: UIViewController {
    
    @IBOutlet weak var chatContainer: UIView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    static var senderId: String?
    static var senderDisplayName: String?
    static var theOtherUser: String?
    
    let manager = Manager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - action method to return to main screen
    
    @IBAction private func back(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        MembersArea.userToPrivateChat = " "
        manager.removeObserverFromServer(child: PrivateChatMainScreen.senderId! + PrivateChatMainScreen.theOtherUser!)
        manager.removeObserverFromServer(child: PrivateChatMainScreen.theOtherUser! + PrivateChatMainScreen.senderId!)
    }
}

class PrivateChat: JSQMessagesViewController, ManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private lazy var messages = [JSQMessage]()
    private var outgoingBubbleImageView: JSQMessagesBubbleImage!
    private var incomingBubbleImageView: JSQMessagesBubbleImage!
    private var theOtherUser: String?
    private var firebaseMessageRef: String?
    private var localTyping = false
    private var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            manager.observeTyping(senderId: senderId, typing: localTyping)
        }
    }
    
    private let manager = Manager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initChat()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func initChat() {
        self.view.backgroundColor = .clear
        collectionView.backgroundColor = .clear
        self.senderId = PrivateChatMainScreen.senderId
        self.senderDisplayName = PrivateChatMainScreen.senderDisplayName
        self.theOtherUser = PrivateChatMainScreen.theOtherUser
        setBubbleColor()
        manager.delegate = self
        if theOtherUser == "messages" {
            firebaseMessageRef = "messages"
            manager.observeMessage(child: firebaseMessageRef!)
        } else {
            firebaseMessageRef = senderId + theOtherUser!
            guard firebaseMessageRef!.contains(senderId) && firebaseMessageRef!.contains(theOtherUser!) else {return}
            manager.observeMessage(child: firebaseMessageRef!)
            firebaseMessageRef = theOtherUser! + senderId
            guard firebaseMessageRef!.contains(senderId) && firebaseMessageRef!.contains(theOtherUser!) else {return}
            manager.observeMessage(child: firebaseMessageRef!)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.item]
        let dateToDisplay = Date().formatDate(message.date)
        if message.senderId == senderId {
            return NSAttributedString(string: dateToDisplay)
        }
        return NSAttributedString(string: message.senderId + "    " + dateToDisplay)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        guard message.isMediaMessage else {
            if message.senderId == senderId {
                cell.textView!.textColor = .black
            } else {
                cell.textView!.textColor = .black
            }
            return cell
        }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        let message = messages[indexPath.item]
        guard message.isMediaMessage  else {return}
        fullScreenImage(message)
    }
    
    //MARK: - monitor if user is typing
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        textView.spellCheckingType = .no
        isTyping = textView.text != ""
    }
    
    //MARK: - send pressed action method
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        isTyping = false
        manager.sendMessage(child: firebaseMessageRef!, text: text, senderId: senderId, date: date)
        finishSendingMessage()
    }
    
    //MARK: - send file action method
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        let errorAlert = UIAlertController(title: title, message: "Choose Action", preferredStyle: UIAlertControllerStyle.alert)
        errorAlert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { [weak self]
            (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        errorAlert.addAction(UIAlertAction(title: "Library", style: .default, handler: { [weak self] (action: UIAlertAction!) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        errorAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        errorAlert.view.alpha = 0.7
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    //MARK: - imagepicker delegate method for selected photo
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let data = UIImageJPEGRepresentation(pickedImage, 0.5)!
            manager.sendFile(data: data, senderId: senderId, text: " ")
        }
    }
    
    //MARK: - set bubble color
    
    private func setBubbleColor() {
        let bubble = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = bubble?.outgoingMessagesBubbleImage(with: #colorLiteral(red: 0.3892424703, green: 0.7117116451, blue: 0.9899192452, alpha: 1))
        incomingBubbleImageView = bubble?.incomingMessagesBubbleImage(with: #colorLiteral(red: 0.2710134387, green: 0.7242972255, blue: 0.2397102118, alpha: 1))
    }
    
    //MARK: - methods for full screen image from bubble
    
    func fullScreenImage(_ message: JSQMessage) {
        let photo = message.media as! JSQPhotoMediaItem
        let photoView = UIImageView(image: photo.image)
        photoView.frame = self.view.frame
        photoView.backgroundColor = .black
        photoView.contentMode = .scaleAspectFit
        photoView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        tap.numberOfTapsRequired = 2
        photoView.addGestureRecognizer(tap)
        self.view.addSubview(photoView)
    }
    
    func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    //MARK: - observe message protocol implementation
    
    func observeMessages(senderId: String, text: String, date: String) {
        let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: Date().dateFromString(date), text: text)
        messages.append(message!)
        messages.sort(by: {$0.date < $1.date})
        finishReceivingMessage()
        JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
    }
    
    func observeMessagesWithData(senderId: String, data: Data, text: String) {
        for txtMessage in messages where !txtMessage.isMediaMessage && txtMessage.text == text {
            let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: txtMessage.date, media: JSQPhotoMediaItem(image: UIImage(data: data)))
            messages.insert(message!, at: messages.index(of: txtMessage)!)
            messages.remove(at: messages.index(of: txtMessage)!)
            break
        }
        finishReceivingMessage()
        JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
    }
    
    //MARK: - observe user typing protocol implementation
    
    func observeTyping(_ count: Int) {
        let userIsTypingCount = count
        if userIsTypingCount == 1 && self.isTyping {
            return
        }
        self.showTypingIndicator = userIsTypingCount > 0
        self.collectionView.typingIndicatorMessageBubbleColor = .green
        self.collectionView.typingIndicatorEllipsisColor = .black
        self.scrollToBottom(animated: true)
    }
}
