//
//  MembersArea.swift
//  Hai In Bucovina
//
//  Created by Alexx on 01/09/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

class MembersArea: UIViewController, UITableViewDelegate, UITableViewDataSource, ManagerDelegate {
    
    
    @IBOutlet weak var onlineUsers: UITableView!
    @IBOutlet weak var conversationsTable: UITableView!
    
    private lazy var userArray: [String] = []
    private lazy var userConversations: [String] = []
    static var userToPrivateChat: String = " "
    let manager = Manager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onlineUsers.delegate = self
        onlineUsers.dataSource = self
        conversationsTable.delegate = self
        conversationsTable.dataSource = self
        manager.delegate = self
        manager.checkLoggedUsers()
        manager.observeCurrentUserConversations(senderId: manager.checkCredentials().1.components(separatedBy: "@").first!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        onlineUsers.reloadData()
        conversationsTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == onlineUsers {
            guard userArray.count == 0 else {
                return userArray.count
            }
        }
        if tableView == conversationsTable {
            guard userConversations.count == 0 else {
                return userConversations.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if tableView == onlineUsers {
            let cellIdentifier = "MembersAreaCell"
            let onlineUserCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MembersAreaCell
            if userArray.count != 0 {
                let user = userArray[indexPath.row]
                if user != " " {
                    onlineUserCell.user?.text = user
                }
            }
            cell = onlineUserCell
        }
        if tableView == conversationsTable {
            let cellIdentifier = "UserConversationsCell"
            let userConvCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! UserConversationsCell
            if userConversations.count != 0 {
                let conversation = userConversations[indexPath.row]
                userConvCell.conversation?.text = conversation
            }
            cell = userConvCell
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == onlineUsers {
            onlineUsers.deselectRow(at: indexPath, animated: false)
            MembersArea.userToPrivateChat = self.userArray[indexPath.row]
            performSegue(withIdentifier: "privateChat", sender: onlineUsers.cellForRow(at: indexPath))
        }
        if tableView == conversationsTable {
            conversationsTable.deselectRow(at: indexPath, animated: false)
            MembersArea.userToPrivateChat = self.userConversations[indexPath.row]
            performSegue(withIdentifier: "privateChat", sender: tableView.cellForRow(at: indexPath))
        }
    }
    
    //MARK: - protocol implementation for users logging in and out
    
    func addUser(_ user: String) {
        userArray.append(user)
        for item in userArray where item == manager.checkCredentials().1.components(separatedBy: "@").first! {
            userArray.remove(at: userArray.index(of: item)!)
        }
        onlineUsers.reloadData()
    }
    
    func removeUser(_ user: String) {
        for item in userArray where item == user {
            userArray.remove(at: userArray.index(of: item)!)
        }
        onlineUsers.reloadData()
    }
    
    //MARK - protocol method for current users conversations
    
    func observeUserConversations(_ users: String) {
        var appendableUserConv: String?
        appendableUserConv = users.replacingOccurrences(of: self.manager.checkCredentials().1.components(separatedBy: "@").first!, with: "")
        guard userConversations.contains(appendableUserConv!) else {
            userConversations.append(appendableUserConv!)
            conversationsTable.reloadData()
            return
        }
    }
    
    //MARK: - back button action
    
    @IBAction func backButton(sender: UIButton) {
        manager.removeObserverFromServer(child: "membersLogged")
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: - segue to chatVC
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "privateChat" {
            super.prepare(for: segue, sender: sender)
            _ = segue.destination as? PrivateChatMainScreen
            PrivateChatMainScreen.senderId = manager.checkCredentials().1.components(separatedBy: "@").first!                                                                           //send name
            PrivateChatMainScreen.senderDisplayName = manager.checkCredentials().0                    //send uid
            guard MembersArea.userToPrivateChat != " " else {
                PrivateChatMainScreen.theOtherUser = "messages"
                return
            }
            PrivateChatMainScreen.theOtherUser = MembersArea.userToPrivateChat
        }
    }
}
