//
//  Events.swift
//  Hai In Bucovina
//
//  Created by Alexx on 04/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation

enum Buttons: String {
    
    case Sports = "sports"
    case Events = "events"
    case About = "about"
    case Nature = "nature"
    case Hiking = "hiking"
}
