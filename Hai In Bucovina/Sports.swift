//
//  Sports.swift
//  Hai In Bucovina
//
//  Created by Alexx on 02/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation


class Sports: NSObject {
    
    static let chute = ("Chute", "http://explorebucovina.com/paragliding/")
    static let ski = ("Ski", "http://www.campulungmoldovenesc.ro/primaria/finantari/partie_schi.pdf")
    static let skiCabin = ("SkiCabin", "http://www.campulungmoldovenesc.ro/primaria/finantari/partie_schi.pdf")
    static let offRoad = ("OffRoad", "http://www.inbucovina.ro/obiective-turistice/sport-extrem/off-road-socialising/")
    static let climbing = ("Climbing", "http://www.fralpinism.ro/topo/TOPO_Rarau.pdf")
    static let cycling = ("Cycling", "http://www.radicalrace.ro")
    static let enduro = ("Enduro", "http://endurobucovina.ro")
    static let drift = ("Drift", "http://www.inbucovina.ro/evenimente/11322/")
    
    static let sportsArray = [chute, ski, skiCabin, offRoad, climbing, cycling, enduro, drift]
    
}

class Events: NSObject {
    
    
    static let film = ("Chute", "http://explorebucovina.com/paragliding/")
    static let intalniri = ("Ski", "http://www.campulungmoldovenesc.ro/primaria/finantari/partie_schi.pdf")
    static let rrrace = ("SkiCabin", "http://www.campulungmoldovenesc.ro/primaria/finantari/partie_schi.pdf")
    static let ziloras = ("Climbing", "http://www.fralpinism.ro/topo/TOPO_Rarau.pdf")
    
}

class Nature: NSObject {
    
    
}

class Hiking: NSObject {
    
    
}

