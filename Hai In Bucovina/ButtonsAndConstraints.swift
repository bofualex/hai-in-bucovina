//
//  Helper.swift
//  Hai In Bucovina
//
//  Created by Alexx on 31/07/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

class ButtonsAndConstraints: NSObject {
    
    //MARK: - set button state and alpha methods
    
    class func setButtonsAlpha(_ button1: [UIButton], alpha: CGFloat) {
        for i in 0..<button1.count {
            button1[i].alpha = alpha
        }
    }
    
    class func setButtonsState(_ button1: [UIButton], enabled: Bool) {
        for i in 0..<button1.count {
            button1[i].isEnabled = enabled
        }
    }
    
    //MARK: - button generation methods

    class func generateButtonsFromFile(_ view: UIView, buttonType: String, backColor: UIColor, dimension: Int) -> [UIButton] {
        var button: [UIButton] = []
        let resourcePath = Bundle.main.resourcePath!
        let imgName = "Images.xcassets"
        let path = resourcePath + "/" + imgName
        let fm = FileManager.default
        let items = try! fm.contentsOfDirectory(atPath: path)
        var objects: [String] = []
        
        for item in items {
            if item.hasPrefix(buttonType) {
                objects.append(item)
            }
        }
        var format: [String] = []
        for item in objects {
            format.append((item.components(separatedBy: CharacterSet.punctuationCharacters).first)!)
        }
        for i in 0..<format.count {
            let name = UIButton()
            name.translatesAutoresizingMaskIntoConstraints = false
            var array: [UIImage] = []
            for item in format
            {
                array.append(UIImage(named: item)!)
            }
            name.setImage(array[i], for: UIControlState())
            name.clipsToBounds = true
            name.layer.cornerRadius = CGFloat(dimension)/2
            name.accessibilityIdentifier = format[i].replacingOccurrences(of: buttonType, with: "")
            name.tag = i
            name.backgroundColor = backColor
            name.alpha = 1
            view.addSubview(name)
            let width = NSLayoutConstraint(item: name, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: CGFloat(dimension))
            let height = NSLayoutConstraint(item: name, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: CGFloat(dimension))
            view.addConstraints([width, height])
            button.append(name)
        }
        return button
    }

    class func generateButtons(_ view: UIView, nrOfButtons: Int) -> [UIButton] {
        var button: [UIButton] = []
        for _ in 0..<nrOfButtons {
            let name = UIButton()
            name.translatesAutoresizingMaskIntoConstraints = false
            name.backgroundColor = .lightGray
            name.clipsToBounds = true
            name.layer.cornerRadius = 8
            name.setTitleColor(.black, for: .normal)
            name.titleLabel?.font = .systemFont(ofSize: 12)
            view.addSubview(name)
            let width = NSLayoutConstraint(item: name, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 74)
            let height = NSLayoutConstraint(item: name, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
            view.addConstraints([width, height])
            button.append(name)
        }
        return button
    }

    //MARK: - set and reset constraints
    
    class func setConstraints(_ constraint1: [NSLayoutConstraint], constraint2: [NSLayoutConstraint], radius: CGFloat) {
        for i in 0..<constraint1.count {
            let angle: CGFloat = (CGFloat)(2*M_PI / Double(constraint1.count))
            constraint1[i].constant += radius * cos(angle * (CGFloat)(constraint1.count - i))
            constraint2[i].constant += radius * sin(angle * (CGFloat)(constraint1.count - i)) + (CGFloat)(i)
        }
    }
    
    class func setConstraints1(_ constraint: [NSLayoutConstraint], radius: CGFloat, x: CGFloat, y: CGFloat) {
        for i in 0..<constraint.count {
            let z = CGFloat((constraint.count + i))
            constraint[i].constant = radius * -sin(0) + x + y*z
        }
    }

    class func resetConstraints(_ constraint: [NSLayoutConstraint], constant: CGFloat) {
        for i in 0..<constraint.count {
            constraint[i].constant = constant
        }
    }
    
    class func generateConstraintsRight(_ view: UIView, button1: [AnyObject], constant: CGFloat) -> [NSLayoutConstraint] {
        var constraintsRight: [NSLayoutConstraint] = []
        for i in 0..<button1.count {
            let pinRight = NSLayoutConstraint(item: button1[i], attribute: .rightMargin, relatedBy: .equal, toItem: view, attribute: .rightMargin, multiplier: 1.0, constant: constant)
            constraintsRight.append(pinRight)
            view.addConstraint(pinRight)
        }
        return constraintsRight
    }

    class func generateConstraintsLeft(_ view: UIView, button1: [AnyObject], constant: CGFloat) -> [NSLayoutConstraint] {
        var constraintsLeft: [NSLayoutConstraint] = []
        for i in 0..<button1.count {
            let pinLeft = NSLayoutConstraint(item: button1[i], attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: constant)
            constraintsLeft.append(pinLeft)
            view.addConstraint(pinLeft)
        }
        return constraintsLeft
    }
    
    class func generateConstraintsTop(_ view: UIView, button1: [AnyObject], constant: CGFloat) -> [NSLayoutConstraint] {
        var constraintsTop: [NSLayoutConstraint] = []
        for i in 0..<button1.count {
            let pinTop = NSLayoutConstraint(item: button1[i], attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: constant)
            constraintsTop.append(pinTop)
            view.addConstraint(pinTop)
        }
        return constraintsTop
    }
}
