//
//  Server.swift
//  Hai In Bucovina
//
//  Created by Alexx on 07/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

typealias responseFromServer = [(user: String, password: String)]

protocol ServerDelegate: class {
    func sendUserData(_ user: String)
    func checkAuth(_ auth: String!)
    func observeUserConversations(_ users: String)
    func observeMessages(senderId: String, text: String, date: String)
    func observeMessagesWithData(senderId: String, data: Data, text: String)
    func observeTyping(_ count: Int)
    func addUser(_ user: String)
    func removeUser(_ user: String)
}

extension ServerDelegate {
    func sendUserData(_ user: String) {
    }
    func checkAuth(_ auth: String!) {
    }
    func observeUserConversations(_ users: String) {
    }
    func observeMessages(senderId: String, text: String, date: String) {
    }
    func observeMessagesWithData(senderId: String, data: Data, text: String) {
    }
    func observeTyping(_ count: Int) {
    }
    func addUser(_ user: String) {
    }
    func removeUser(_ user: String) {
    }
}


class Server: NSObject {
    
    private let rootRef = FIRDatabase.database().reference()
    private let storageRef = FIRStorage.storage().reference(forURL: "gs://hai-in-bucovina.appspot.com")
    
    private var messageRef: FIRDatabaseReference?
    private var imageRef: FIRStorageReference?
    private var userIsTyping: FIRDatabaseReference?
    private var usersTypingQuery: FIRDatabaseQuery?
    
    weak var delegate: ServerDelegate?
    
    
    func userHasLoggedIn(_ email: String) {
        let usersRef = rootRef.child("membersLogged")
        let loggedRef = usersRef.child(email)
        loggedRef.setValue(1)
        checkLoggedUsers()
    }
    
    func userHasLoggedOut(_ email: String) {
        let usersRef = rootRef.child("membersLogged")
        let removeChild = usersRef.child(email)
        removeChild.removeValue()
        checkLoggedUsers()
    }
    
    func checkLoggedUsers() {
        let usersRef = rootRef.child("membersLogged")
        let usersQuery = usersRef.queryOrderedByKey()
        DispatchQueue.global(qos: .background).async { [weak self] in
            usersQuery.observe(.childAdded) { [weak self] (snapshot: FIRDataSnapshot!) in
                DispatchQueue.main.async {
                    let users = snapshot.key
                    self?.delegate?.addUser(users)
                }
            }
        }
        DispatchQueue.global(qos: .background).async { [weak self] in
            usersQuery.observe(.childRemoved) { [weak self] (snapshot: FIRDataSnapshot!) in
                DispatchQueue.main.async {
                    let users = snapshot.key
                    self?.delegate?.removeUser(users)
                }
            }
        }
    }
    
    func login(email: String, password: String) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            FIRAuth.auth()?.signIn(withEmail: email, password: password) { [weak self] (user, error) in
                DispatchQueue.main.async {
                    if (error != nil) {
                        self?.delegate?.checkAuth("\(error!.localizedDescription)")
                        print(" ||  error is \(error!.localizedDescription)")
                    } else {
                        print(" ||  user is \(user?.email)")
                        self?.userHasLoggedIn(email.components(separatedBy: "@").first!)
                        self?.delegate?.checkAuth("success")
                    }
                }
            }
        }
    }
    
    func createUser(email: String, password: String) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            FIRAuth.auth()?.createUser(withEmail: email, password: password) { (user, error) in
                DispatchQueue.main.async {
                    if error == nil {
                        print(" ||  success")
                        self?.login(email: email, password: password)
                        self?.userHasLoggedIn(email.components(separatedBy: "@").first!)
                    } else {
                        print(" ||  error exists \(error?.localizedDescription)")
                    }
                }
            }
        }
    }
    
    func signOut(_ email: String) {
        try! FIRAuth.auth()!.signOut()
        userHasLoggedOut(email.components(separatedBy: "@").first!)
        print(" ||  user is logged out")
    }
    
    func listenForUserAuth() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            FIRAuth.auth()!.addStateDidChangeListener() { (auth, user) in
                DispatchQueue.main.async {
                    guard let user = user else {
                        print(" || No user is signed in. Why?")
                        self?.delegate?.sendUserData("not logged in")
                        return
                    }
                    print(" || User is signed in with uid: \(user.uid)")
                    self?.delegate?.sendUserData(user.email!)
                    self?.userHasLoggedIn(user.email!.components(separatedBy: "@").first!)
                }
            }
        }
    }
    
    func sendCredentialsForAuthUser() -> (id: String, name: String) {
        let id = FIRAuth.auth()?.currentUser?.uid ?? "no id no user"
        let name = FIRAuth.auth()?.currentUser?.email ?? "anon@anon"
        return (id, name)
    }
    
    func sendMessage(child: String, text: String, senderId: String, date: Date) {
        DispatchQueue.global(qos: .background).async { [weak self] in
        self?.messageRef = self?.rootRef.child(child)
        let itemRef = self?.messageRef?.childByAutoId()
        let dateToString = date.description
        let messageItem = ["text": text, "senderId": senderId, "date": dateToString] as [String: Any]
        itemRef?.setValue(messageItem)
        }
    }
    
    func sendFile(data: Data, senderId: String, text: String) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            let setUniqueId = Date().timeIntervalSince1970.description
            let url = "gs://hai-in-bucovina.appspot.com/photo/\(senderId)/\(setUniqueId)"
            let photoRef = self?.storageRef.child("/photo/\(senderId)/\(setUniqueId)")
            let dateToString = Date().description
            photoRef?.put(data).observe(.success) { (snapshot: FIRStorageTaskSnapshot!) in
                let messageItem = ["text": text, "url": url, "senderId": senderId, "date": dateToString] as [String : Any]
                DispatchQueue.main.async {
                    self?.rootRef.child("messages").childByAutoId().setValue(messageItem)
                }
            }
        }
    }
    
    func observeCurrentUserConversations(senderId: String) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.rootRef.observe(.childAdded) {
                (snapshot: FIRDataSnapshot) in
                let conversations = snapshot.key
                if conversations.contains(senderId) {
                    DispatchQueue.main.async {
                        self?.delegate?.observeUserConversations(conversations)
                    }
                }
            }
        }
    }
    
    func observeMedia(_ url: String, senderId: String) {
        DispatchQueue.global(qos: .background).async { [weak self] in
        FIRStorage.storage().reference(forURL: url).data(withMaxSize: Int64.max, completion: { data,error -> Void in
            if (error != nil) {
                print(" ||  \(error?.localizedDescription)")
            } else {
                DispatchQueue.main.async {
                self?.delegate?.observeMessagesWithData(senderId: senderId, data: data!, text: url)
                }
            }
            })
        }
    }
    
    func observeMessage(child: String) {
        self.messageRef = self.rootRef.child(child)
        let messagesQuery = self.messageRef?.queryLimited(toLast: 25)
        DispatchQueue.global(qos: .background).async { [weak self] in
            messagesQuery?.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
                let message = snapshot.value as! [String: Any]
                let date = message["date"] as! String!
                let senderId = message["senderId"] as! String!
                let text = message["text"] as! String!
                let url = message["url"] as! String!
                DispatchQueue.main.async {
                guard text == " " else {
                    self?.delegate?.observeMessages(senderId: senderId!, text: text!, date: date!)
                    return
                    }
                self?.delegate?.observeMessages(senderId: senderId!, text: url!, date: date!)
                self?.observeMedia(url!, senderId: senderId!)
                }
            }
        }
    }
    
    func observeTyping(senderId: String, typing: Bool) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            let typingIndicator = self?.rootRef.child("typingIndicator")
            self?.userIsTyping = typingIndicator?.child(senderId)
            self?.userIsTyping?.setValue(typing)
            self?.userIsTyping?.onDisconnectRemoveValue()
            self?.usersTypingQuery = typingIndicator?.queryOrderedByValue().queryEqual(toValue: 1)
            self?.usersTypingQuery?.observe(.value) { (data: FIRDataSnapshot!) in
                DispatchQueue.main.async {
                    self?.delegate?.observeTyping(Int(data.childrenCount))
                }
            }
        }
    }
    
    func removeObservers(child: String) {
        rootRef.removeAllObservers()
        rootRef.child("messages").removeAllObservers()
        rootRef.child(child).removeAllObservers()
        rootRef.child("typingIndicator").removeAllObservers()
    }
}
