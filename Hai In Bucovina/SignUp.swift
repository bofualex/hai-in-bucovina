//
//  SignUp.swift
//  Hai In Bucovina
//
//  Created by Alexx on 06/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import UIKit

class SignUp: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    private let manager = Manager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtUsername.setBottomBorder()
        txtPassword.setBottomBorder()
        txtConfirmPassword.setBottomBorder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - check if the credentials are entered correctly
    
    private func checkForEmptyString() -> Bool {
        if txtUsername.text == "" {
            ChildControllers.errorAlert(self, title: "Empty UserName", message: "Please enter username")
            return false
        }
        if txtPassword.text == "" || txtConfirmPassword.text == "" {
            ChildControllers.errorAlert(self, title: "Empty Password", message: "Please enter password")
            return false
        }
        if (txtUsername.text?.characters.count)! < 7 || (txtPassword.text?.characters.count)! < 7 {
            ChildControllers.errorAlert(self, title: "Too short", message: "Please enter longer credentials")
            return false
        }
        if txtPassword.text != txtConfirmPassword.text {
            ChildControllers.errorAlert(self, title: "Passwords don't match", message: "Please re-enter longer passwords")
            return false
        }
        return true
    }
    
    //MARK: - action methods
    
    @IBAction private func signUpTapped(_ sender: UIButton) {
        guard checkForEmptyString() else {return}
        manager.signUp(email: txtUsername.text!, password: txtPassword.text!)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func gotoLogin(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
}

