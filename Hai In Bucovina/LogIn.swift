//
//  Login.swift
//  Hai In Bucovina
//
//  Created by Alexx on 06/08/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import UIKit

class LogIn: UIViewController, ManagerDelegate {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    
    private let manager = Manager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPassword.setBottomBorder()
        txtUsername.setBottomBorder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - check if the credentials are entered correctly
    
    private func checkForEmptyString() -> Bool {
        if txtUsername.text == "" {
            ChildControllers.errorAlert(self, title: "Empty UserName", message: "Please enter username")
            return false
        }
        if txtPassword.text == "" {
            ChildControllers.errorAlert(self, title: "Empty Password", message: "Please enter password")
            return false
        }
        if (txtUsername.text?.characters.count)! < 8 || (txtPassword.text?.characters.count)! < 7 {
            ChildControllers.errorAlert(self, title: "Too short", message: "Please enter longer credentials")
            return false
        }
        return true
    }
    
    //MARK: - action methods
    
    @IBAction private func logInTapped(_ sender: UIButton) {
        guard checkForEmptyString() else {return}
        manager.delegate = self
        manager.login(email: txtUsername.text!, password: txtPassword.text!)
    }
    
    @IBAction private func returnToMain(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - manager protocol for error checking
    
    func checkAuth(_ auth: String!) {
        let auth: String! = auth
        print("||  \(auth)")
        if auth == "success" {
            self.dismiss(animated: true, completion: nil)
        } else {
            ChildControllers.errorAlert(self, title: "An Error Has Occured", message: auth)
        }
    }
}

extension UITextField
{
    func setBottomBorder()
    {
        self.borderStyle = UITextBorderStyle.none
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
