//
//  UserConversationsCell.swift
//  Hai In Bucovina
//
//  Created by Alexx on 03/09/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

class UserConversationsCell: UITableViewCell {
    
    @IBOutlet weak var conversation: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
