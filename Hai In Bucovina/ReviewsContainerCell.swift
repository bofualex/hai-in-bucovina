//
//  ReviewsContainerCell.swift
//  Hai In Bucovina
//
//  Created by Alexx on 06/09/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

class ReviewsContainerCell: UITableViewCell {
    
    @IBOutlet weak var nameReview: UILabel?
    @IBOutlet weak var textReview: UILabel?
    @IBOutlet weak var dateReview: UILabel?
    @IBOutlet weak var moreButton: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textReview?.numberOfLines = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

}
