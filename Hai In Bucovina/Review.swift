//
//  About.swift
//  Hai In Bucovina
//
//  Created by Alexx on 06/09/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit


class Review: UIViewController, WriteReviewDelegate {
    
    @IBOutlet private weak var reviewContainer: UIView?
    @IBOutlet private weak var textContainer: UIView?
    @IBOutlet private weak var writeButton: UIBarButtonItem?
    @IBOutlet private weak var readButton: UIBarButtonItem?
    
    private lazy var buttons: [UIButton] = []
    private lazy var constraintsTop: [NSLayoutConstraint] = []
    private lazy var constraintsRight: [NSLayoutConstraint] = []
    
    private var buttonState: Int = 1
    private let manager = Manager()
    private let managerWriteReview = WriteReview()
    static let sharedInstance = Review()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        managerWriteReview.delegate = self
        managerWriteReview.reloadViewController()
        textContainer?.alpha = 0
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - read/write views action
    
    @IBAction func showContainer(sender: UIBarButtonItem) {
        switch sender.tag {
        case 1:
            UIView.animate(withDuration: 0.5, animations: { [unowned self] in
                self.reviewContainer?.alpha = 1
                self.textContainer?.alpha = 0
                })
        default :
            UIView.animate(withDuration: 0.5, animations: { [unowned self] in
                self.reviewContainer?.alpha = 0
                self.textContainer?.alpha = 1
                })
        }
    }

    //MARK: - back button action
    
    @IBAction func backButton(sender: UIButton) {
        ChildControllers.dismissChild(currentView: self)
        manager.removeObserverFromServer(child: "anonym")
    }
    
    func reloadReview(_ text: String) {
        self.textContainer?.alpha = 0
        self.reviewContainer?.alpha = 1
        //self.view.setNeedsDisplay()
    }
}

